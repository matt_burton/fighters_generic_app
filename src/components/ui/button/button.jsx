import React from 'react'

import styles from './button.module.scss'

const Button = ({ type, clickEvent, label, classes }) => {
    let classArr = [];
    classes.map(theClass => {
        return classArr.push(styles[theClass])
    })

    return <button type={type} onClick={clickEvent} className={classArr.join(' ')}>{label}</button>
}
export default Button