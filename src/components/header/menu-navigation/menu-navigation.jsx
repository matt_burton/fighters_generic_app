import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { userLogout } from '../../../actions/userActions'
import { toggleMenu } from '../../../actions/globalActions'

import Button from '../../ui/button/button'


import styles from './menu-navigation.module.scss'

class MenuNavigation extends Component {

    constructor(){
        super()
        this.state = {
    
        }
    }

    render(){
        const { username, isAdmin } = this.props.user
        const { titles, theme } = this.props.global
    
        return (
            <div className={styles.menuNavigation}>
                        <nav>
                            {username && isAdmin &&
                                <>
                                    <Link onClick={() => this.props.toggleMenu('close')} to="/controller" className={(titles.primary === 'Admin' ? styles.active : '')}>Admin</Link>
                                </>
                            }
                            {username && !isAdmin &&
                                <>
                                    <Link onClick={() => this.props.toggleMenu('close')} to="/" className={(titles.primary === 'Agenda' ? styles.active : '')}>Home</Link>
                                 
                                </>
                            }
                            {username &&
                                <Button type="button" classes={['logout', (theme === 'red' ? 'invertedLogout' : '')]} clickEvent={userLogout} label="Logout" />
                            }
                        </nav>
                     
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
    global: state.global
})

export default connect(mapStateToProps, { userLogout, toggleMenu })(MenuNavigation)