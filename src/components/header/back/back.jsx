import React from 'react'
import { withRouter } from 'react-router-dom'

import { ReactComponent as BackIcon } from '../../../assets/images/ui/back.svg'

import styles from './back.module.scss'

const Back = ({ username, history }) => (
    <div className={styles.back}>
    {username && window.location.pathname !== '/' &&
        <p onClick={history.goBack}><BackIcon /></p>
    }
    </div>
)
export default withRouter(Back)