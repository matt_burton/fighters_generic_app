import React, { Component } from 'react'

import "./message.scss";

const Message = ({message, messageType }) => {
   
    return (
        <div className="message-list">
         {/* <pre>{JSON.stringify(results, null, 2) }</pre> */}

                 <div className={messageType}>
                        {message}
                 </div>
                 
        </div>
    )
}

export default Message;