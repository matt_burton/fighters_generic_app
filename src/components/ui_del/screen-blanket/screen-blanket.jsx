import React from 'react'
import { CSSTransition } from 'react-transition-group'
import { connect } from 'react-redux'

import { toggleMenu } from '../../../actions/globalActions'

import styles from './screen-blanket.module.scss'

const ScreenBlanket = ({ isMenuOpen, toggleMenu, allQuestions, global, presentation }) => {
    
    const { titles } = global
    const { presentationType } = presentation

    const transitionFade = {
        appear:'fade-enter',
        enter: 'fade-enter',
        appearActive: 'fade-enter-active',
        enterActive: 'fade-enter-active',
        exit: 'fade-exit',
        exitActive: 'fade-exit-active'
    }

    const isPromoted = (allQuestions && allQuestions.filter(({promoted}, index) => promoted).length > 0 ? true : false)
    const toggleFlag = ( isMenuOpen || isPromoted && titles.primary === 'Presentation' && presentationType === 'questions' )

    return (
        <CSSTransition
            in={toggleFlag}
            timeout={500}
            classNames={transitionFade}
            unmountOnExit
        >
            <div className={styles.screenBlanket} onClick={() => toggleMenu('close')}></div>
        </CSSTransition>
    )
}

const mapStateToProps = state => ({
    isMenuOpen: state.global.isMenuOpen,
    allQuestions: state.userquestions.allquestions,
    global: state.global,
    presentation: state.presentation
})

export default connect(mapStateToProps, { toggleMenu })(ScreenBlanket)