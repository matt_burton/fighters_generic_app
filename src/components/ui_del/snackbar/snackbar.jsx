import React from 'react'
import { connect } from 'react-redux'
import { CSSTransition } from 'react-transition-group'

import styles from './snackbar.module.scss'

const Snackbar = ({ snackbar }) => {

    const { currentStatus, message, type } = snackbar

    const soClassy = {
        appear: styles['snack-appear'],
        appearActive: styles['snack-appear-active'],
        enter: styles['snack-enter'],
        enterActive: styles['snack-enter-active'],
        exit: styles['snack-exit'],
        exitActive: styles['snack-exit-active']
    }
   
    return (
        <CSSTransition
            in={currentStatus}
            timeout={350}
            classNames={soClassy}
            appear
            unmountOnExit
        >
            <section key="snackbar" className={[styles.snackbar, styles[type]].join(' ')}>
                <p>{message}</p>
            </section>
        </CSSTransition>
    )
}

const mapStateToProps = state => ({
    snackbar: state.snackbar
})


export default connect(mapStateToProps, {})(Snackbar)