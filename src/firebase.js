import firebase from 'firebase'
const config = {
  apiKey: "AIzaSyCiGi4TKIXTp0H2YPdpkZtjmelhPqAZgFw",
  authDomain: "teamfight-8ee60.firebaseapp.com",
  databaseURL: "https://teamfight-8ee60.firebaseio.com",
  projectId: "teamfight-8ee60",
  storageBucket: "teamfight-8ee60.appspot.com",
  messagingSenderId: "584568935840",
  appId: "1:584568935840:web:eebefeb9edee149ae300c5"
};
firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;