
export const SET_USERS     = 'SET_USERS'
export const SET_USERLEVEL = 'SET_USERS'
export const USER_LOGOUT   = 'USER_LOGOUT'

// Global
export const SET_PAGETITLE = 'SET_PAGETITLE'
export const TOGGLE_MENU   = 'TOGGLE_MENU'
export const SET_MESSAGE   = 'SET_MESSAGE'
//Disciplines
export const SET_DISCIPLINES = 'GET_DISCIPLINES'

export const CURRENT_SLIDE = 'CURRENT_SLIDE'
