import {
    SET_PAGETITLE,
    TOGGLE_MENU,
    CURRENT_SLIDE,
    SET_MESSAGE
} from './types'

export const setPageTitle = (data) => {
    // document.title = data.titles.primary
    return {
        type: SET_PAGETITLE,
        payload: data
    }
}

export const setMessage = (data) => {
    // document.title = data.titles.primary
    return {
        type: SET_MESSAGE,
        payload: data
    }
}


export const toggleMenu = (setStatus = null) => {
    return {
        type: TOGGLE_MENU,
        payload: setStatus
    }
}

export const currentSlide = () => {
    return {
        type: CURRENT_SLIDE
    }
}