import { 
    SET_USERS,
    SET_USERLEVEL,
    USER_LOGOUT
 } from './types'
 import  { auth } from '../firebase.js';
 
 export function setUsers (data) {
    return {
        type: SET_USERS,
        data: data
    }
}

export function setUserLevel (username, userlevel, details) {
    return {
        type: SET_USERLEVEL,
        username: username,
        userlevel: userlevel,
        details: details
    }
}
 
export const userLogout = async event => {
    await auth.signOut()
    window.location.href = './'
    return {
        type: USER_LOGOUT
    }
}