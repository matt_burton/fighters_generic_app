import {store} from "../../store"
import {setDisciplines} from "../../actions/disciplines"

import firebase from '../../firebase.js';



export const setAllDisciplines = () => {
      
    let alldisciplines = firebase.firestore().collection('disciplines'); //Get reference from firebase
    alldisciplines
  
    .onSnapshot(function(querySnapshot) {
        
        let alldisciplines = []; let i =0;
        querySnapshot.forEach(function(doc) {
            alldisciplines.push(doc.data());
             alldisciplines[i]['id'] = doc.id;
            i++;
        })

        store.dispatch(setDisciplines(alldisciplines));
    })
  


}


