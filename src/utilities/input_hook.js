import  { useState } from 'react';


const handleChange = initialValue => {

    const [ value, setValue ] = useState('')

    return {
        value,
        reset: () => {
            setValue('')
        },
        bind: {
            value, 
            onChange: e => {
                setValue(e.target.value);
            }
        }

    }
}

export default handleChange