import { combineReducers } from 'redux'

import userReducer from './userReducer'
import globalReducer from './globalReducer'
import disciplinesReducer from './disciplinesReducer'


export default combineReducers({

	user: userReducer,
	global: globalReducer,
	discipline: disciplinesReducer

})