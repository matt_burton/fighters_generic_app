import { 
    SET_USERLEVEL,
    USER_LOGOUT
} from '../actions/types'

const initialState = {
    username: '',
    userlevel: '',
    isAdmin: false,
    details: []
}

export default function(state = initialState, action){
    switch (action.type) {
        case SET_USERLEVEL:
            const isAdmin = (action.userlevel === 'admin' ? true : false)
            return {
                ...state,
                username: action.username,
                userlevel: action.userlevel,
                isAdmin: isAdmin,
                details: action.details
            }
   
        case USER_LOGOUT:
            return {
                ...state,
                ...initialState
            }
        default:
            return state;
    }

}