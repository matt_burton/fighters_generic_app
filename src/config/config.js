import { store } from '../store'
import { setPageTitle } from '../actions/globalActions'


export const formData = [
    {disciplines : ['title','description']}
]



export const conferenceData = {
    client: 'Pfizer',
    name: 'SHAPE AfME',
    location: '',
    dates: '',
    welcomeText: `<h1>
        Welcome to SHAPE AfME
    </h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sodales interdum elit ac tincidunt. Praesent malesuada enim ut nunc condimentum convallis. 
    Cras blandit velit non augue elementum porta. Sed rhoncus aliquet massa vitae pellentesque. Ut tincidunt nunc non magna laoreet egestas. 
    Nulla pharetra mattis urna et lacinia. Phasellus fermentum nisi et ante facilisis egestas. Sed bibendum non arcu at sollicitudin. Integer lobortis vulputate auctor.</p>`
}

export const pageTitles = (pathname) => {

    const titleMap = [
        {
            route: '/',
            titles: {
                primary: 'Dashboard'
            }
        },
        {
            route: '/agenda',
            titles: {
                primary: 'Agenda',
                secondary: 'Day one'
            },
            theme: 'red'
        },
        {
            route: '/speakers',
            titles: {
                primary: 'Speakers'
            }
        },
        {
            route: '/livepoll',
            titles: {
                primary: 'Live poll'
            }
        },
        {
            route: '/question/ask',
            titles: {
                primary: 'Ask a question'
            }
        },
        {
            route: '/moderation',
            titles: {
                primary: 'Moderate questions'
            }
        },
        {
            route: '/presentation',
            titles: {
                primary: 'Presentation'
            }
        },
        {
            route: '/controller',
            titles: {
                primary: 'Admin'
            }
        },
        {
            route: '/admin/agenda',
            titles: {
                primary: 'Admin agenda'
            },
            theme: 'red'
        },
        {
            route: '/admin/speaker',
            titles: {
                primary: 'Admin speakers'
            },
        },
        {
            route: '/polls',
            titles: {
                primary: 'Admin polls'
            },
        },
    ]

    // Locates the route based on the pathname
    const theRoute = titleMap.filter(route => route.route === pathname)[0]

    store.dispatch(setPageTitle(theRoute))
 
}

export const numberToText = number => {
    const textMap = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']
    return textMap[number]
}

export const indexToLetter = index => {
    const textMap = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    return textMap[index]
}