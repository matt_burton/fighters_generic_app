import React, { Component } from 'react'
import { connect } from 'react-redux'

import DisciplinesAdd  from './components/details/disciplinesAdmin'

// import styles from './Controller.module.scss'


class Admin extends Component {

	constructor(props) {
		super(props);
		this.state = {
			currentItem : 'disciplines'
		}
	}
	componentDidMount() {
	}

	render(){
		return(
			<>
			<h3>Admin</h3>
			<DisciplinesAdd />
			</>
		)
	}
}
const mapStateToProps = state => ({
	user: state.user,

})

export default connect(mapStateToProps, {})(Admin)