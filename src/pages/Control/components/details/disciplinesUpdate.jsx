import React, { Component } from 'react'
import { connect } from 'react-redux'
import firebase from '../../../../firebase.js';
import {setMessage} from "../../../../actions/globalActions"
import {store} from "../../../../store"
import {formData} from "../../../../config/config"

 class DisciplinesUpdate extends Component {
    constructor(props) {
		super(props);
		this.state = {
            singleDist : '',
            id: ''
        }
        formData[0].disciplines.map((value) => {
            this.state[value] = '';
        })
         this.handleChange = this.handleChange.bind(this);
         this.updateDiscipline = this.updateDiscipline.bind(this);
    }



    updateDiscipline(event){
        event.preventDefault();
        let discipline = firebase.firestore().collection('disciplines').doc(this.state.id);
        //combine all states
        let states = {};
        formData[0].disciplines.map((value) => {
           states[value] = this.state[value];
        })
        discipline.set(states)
        store.dispatch(setMessage({message: 'Updated..'}));
    }

    deleteItem(id){
        let discipline = firebase.firestore().collection('disciplines').doc(this.state.id);
        discipline.delete().then(function(){
            store.dispatch(setMessage({message: 'Deleted..'}));
        })
    }

    handleChange(event) {
        let name = event.target.name;
        this.setState({ [name]: event.target.value});
    }
    
    componentDidUpdate() {
        const {description, title, id} = this.props.singleDist;
        if(this.props.singleDist != this.state.singleDist){
            this.setState(
                {description: description, title: title, id, id,  singleDist: this.props.singleDist}
            )
        }
      }

    render() {
        let formDataRet = [];
        formData[0].disciplines.map((value) => {
            formDataRet.push(<><label >{value}:</label><input type="text" name={value} value={this.state[value]} onChange={this.handleChange} /></>);
        })
        return(
            <>
            <h2>Name</h2>
            <form onSubmit={this.updateDiscipline}>
                    {formDataRet.map(function(item, i){
                    return( item )
                    })}
					<button type="submit"  id="update">Update</button>
					<button type="button"  onClick={() => this.deleteItem(this.state.id)}>Delete</button>
				</form>
            </>
        )
    }
}

    const mapStateToProps = state => ({
        user: state.user,
        disciplines: state.discipline
    })

    export default connect(mapStateToProps)(DisciplinesUpdate)
        