import React, { Component } from 'react'
import { connect } from 'react-redux'
import firebase from '../../../../firebase.js';
import {setMessage} from "../../../../actions/globalActions"
import {store} from "../../../../store"
import {formData} from "../../../../config/config"

 class AddForm extends Component {
    constructor(props) {
		super(props);
		this.state = {
            singleDist : '',
            id: ''
        }

        formData[0].disciplines.map((value) => {
            this.state[value] = '';
        })

         this.handleChange = this.handleChange.bind(this);
         this.updateDiscipline = this.updateDiscipline.bind(this);
    }

    updateDiscipline(event){
        event.preventDefault();
        let discipline = firebase.firestore().collection('disciplines');
        let states = {};
        formData[0].disciplines.map((value) => {
           states[value] = this.state[value];
        })
        discipline.add(states)
        store.dispatch(setMessage({message: 'Add new..'}));
    }

    handleChange(event) {
        let name = event.target.name;
        this.setState({ [name]: event.target.value});
    }

    render() {
        let formDataRet = [];
        formData[0].disciplines.map((value) => {
            formDataRet.push(<><label >{value}:</label><input type="text" name={value} value={this.state[value]} onChange={this.handleChange} /></>);
        })
        return(
            <>
            <h2>Name</h2>
            <form onSubmit={this.updateDiscipline}>
            {formDataRet.map(function(item, i){
                    return( item )
                    })}
						<button type="submit"  id="update">Add Discipline</button>
						{/* <button type="button" class="btn btn-danger" id="delete">Delete</button> */}
				
				</form>
            </>
        )
    }
}

    const mapStateToProps = state => ({
        user: state.user,
        disciplines: state.discipline
    })

    export default connect(mapStateToProps)(AddForm)
        