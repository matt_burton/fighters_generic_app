import React from 'react';
import { connect } from 'react-redux'

import { setcurrent_fb } from '../../utilities/speakers/setCurrent'

import styles from './currentSpeaker.module.scss'

const CurSpeaker = ({ speaker, setcurrent_fb }) => (
    <div className={styles.speakerList}>
        <ul>
            {speaker.map(({ isCurrent, Name, imgUrl, role } , index) => (
                <li key={index} className={isCurrent ? styles.active : '' } onClick={() => setcurrent_fb(Name)}>
                    <div className={styles.flexSpeaker}>
                        <div className={styles.imgHolder} style={{backgroundImage: `url(${imgUrl})`}}></div>
                        <div className={styles.textContainer}>
                            <p>{Name}</p>
                            <small>{role}</small>
                        </div>
                    </div>
                </li>
            ))}
        </ul>
        <button type="button" onClick={() => setcurrent_fb('Reset')}> > Reset </button>
    </div>
   
)

const mapStateToProps = state => ({
    speaker: state.speakers.speaker
})

export default connect(mapStateToProps, { setcurrent_fb })(CurSpeaker)