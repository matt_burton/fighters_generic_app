import React from 'react'
import { connect } from 'react-redux'
import { auth } from '../../../firebase.js'

import handleChange from '../../../utilities/input_hook'
import Input from '../../../components/forms/input'

import { setUserlevelcons, setUserDetails } from '../../../utilities' //Initial user setup

import Button from '../../../components/ui/button/button'

import styles from '../Form.module.scss'

const Login = () => {
	const { value: username, bind: bindEmail, reset: resetUsername } = handleChange('');
	const { value: password, bind: bindPassword, reset: resetPassword } = handleChange('');

	const reset = (e) => {
		resetUsername()
		resetPassword()
	}

	const handleSubmit = (e) => {
		e.preventDefault()
		auth.signInWithEmailAndPassword(username,password) 
			.then((result) => {
			//Set user level based on username
			//setUserlevelcons(username); //Set current user details
			setUserDetails(username)
	
			}).catch(err => {
				alert(err.message);
			});
	}




		return (
				<div key="loginForm" className={styles.formContainer}>
					<div className="flex">
						<div className={styles.textContainer}>
							<h2>Login</h2>
							<p>Please login or register</p>
						</div>
					</div>
					<form onSubmit={handleSubmit} className={styles.theForm} autoComplete="off">
							<label htmlFor="loginUsername">Username</label>			
							<Input type="email" label="Email" fieldName="email" value={username} {...bindEmail} />
							<label htmlFor="loginPassword">Password</label>	
							<Input type="password" label="Password" fieldName="password" value={password} {...bindPassword} />
						<Button label="Login" type="submit" classes={['blue']} />
					</form>
					<Button type="button" label="Register" classes={['blue', 'ghost']}  />
					<button  onClick={reset}>Reset</button>
				</div>
		)
	}


const mapStateToProps = state => ({
	user: state.user
})

export default connect(mapStateToProps, {})(Login)