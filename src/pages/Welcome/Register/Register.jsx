import React from 'react'
import { connect } from 'react-redux'
import { auth } from '../../../firebase.js'

import handleChange from '../../../utilities/input_hook'
import Input from '../../../components/forms/input'

import Button from '../../../components/ui/button/button'

import styles from '../Form.module.scss'

const Register = () => {
	const { value: username, bind: bindEmail, reset: resetUsername } = handleChange('');
	const { value: password, bind: bindPassword, reset: resetPassword } = handleChange('');

	const reset = (e) => {
		resetUsername()
		resetPassword()
	}

	const register = e => {
		e.preventDefault();
		auth.createUserWithEmailAndPassword(username, password)
		.then((result) => {
			//Get user details where email = submitted email ********Neeed to expand this!**************
		}).catch(err => {
			alert(err.message);
		});
	}


		return (
		
				<div key="registerForm" className={styles.formContainer}>
					<div className="flex">
						<div className={styles.textContainer}>
							<h2>Register</h2>
							<p>Register to start using the app.</p>
						</div>
					</div>
					<form onSubmit={register} className={styles.theForm} autoComplete="off">
					{/* <form className={styles.theForm} autoComplete="off"> */}
						<label htmlFor="registerUsername">Email address</label>			
							<Input type="email" label="Email" fieldName="email" value={username} {...bindEmail} />
							<label htmlFor="registerPassword">Password</label>			
							<Input type="password" label="Password" fieldName="password" value={password} {...bindPassword} />
						<Button type="submit" label="Register" classes={['blue']} />
		
						<button  onClick={reset}>Reset</button>
					</form>
				
				</div>
		)
	}


const mapStateToProps = state => ({
	user: state.user
})

export default connect(mapStateToProps, {})(Register)