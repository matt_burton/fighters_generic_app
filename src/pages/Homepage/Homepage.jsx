import React, { Component } from 'react'
import { connect } from 'react-redux'

import HomePagewelcome from './components/defaultHomepage';
import UserFormstyled from './components/userForm';

import { pageTitles } from '../../config/config'


class Details extends Component {
    constructor(props) {
		super(props);
		this.state = {
			showupdate: false, 
			currentId: '',
			hideShow: true
		}
    }


	render() {

		const { username, userlevel, details } = this.props.user

		// console.log(this.props.user, username);
		pageTitles(window.location.pathname)
		let view;
		
		if (this.props.user.userlevel === "nouserdetails")  {
			view = <UserFormstyled  />
		} else{ 
			view = <HomePagewelcome {...{details}}/>
		}

		return (
			<section >
				{view}
			</section>
		)
	}
}

const mapStateToProps = state => ({
	user: state.user
})


export default connect(mapStateToProps)(Details)