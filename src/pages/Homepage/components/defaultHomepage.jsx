import React from 'react'
import { connect } from 'react-redux'


import styles from '../Homepage.module.scss'

const HomePagewelcome = ({details}) => {
    return (
        <>   
        <h1>Welcome {details.Name}</h1>
         <img alt="Welcome -alt..." className={styles.welcomeImage} src="https://firebasestorage.googleapis.com/v0/b/polling-18e3b.appspot.com/o/speakers%2Fc1c237a5-ed1f-4a65-bd9a-d4a4f7be2232.png?alt=media&token=72aad1d1-8457-4f76-9cb5-4b77ce8c4eda"/>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat sit amet sapien at mattis.</p>
                <p>esuada fames ac ante ipsum primis in faucibus. In ornare ut lacus a varius. Vivamus fermentum orci sit amet nulla eleifend accumsan.</p>
                <p>Nam ac sem laoreet, molestie libero eu, tristique tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur nec efficitur leo, ut scelerisque elit.</p>
                <p>Fusce justo dolor, aliquet eu porta vel, semper eu orci. Phasellus tempor, nunc ac ullamcorper rutrum, ligula tortor interdum dolor, quis ornare lorem orci eu ante. Etiam posuere iaculis euismod. Fusce ultricies facilisis interdum.</p>
         </>
    )}

const mapStateToProps = state => ({
    user: state.user
})

export default connect(mapStateToProps, {})(HomePagewelcome)