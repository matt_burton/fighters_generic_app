import React, { Component } from 'react'
import { connect } from 'react-redux'
import firebase from '../../../firebase.js';
import { setUserDetails } from '../../../utilities' //Initial user setup
import  UserFormdisciplines  from './initialFormcomponents/discipline'
import {Message} from '../../../components/common'
 class UserFormstyled extends Component {

   constructor(props) {
		super(props);
		this.state = {
            Name: '',
            disciplines : [],
            email: props.user.username,
            message: '', messageType: 'warning'
        }
        this.selectCheckbox = this.selectCheckbox.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    
    handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		});
    }
    selectCheckbox(value){
        let curState = this.state.disciplines;
        curState.push({'discipline' : value})
        this.setState({
            disciplines: curState
        })
    }
    addBelt(index, event){
        let currentDiscipline = this.state.disciplines;
        currentDiscipline[index][event.target.name] = event.target.value;
        this.setState({
            disciplines: currentDiscipline
        })
    }
    handleSubmit(e){
        e.preventDefault();
        if(this.state.Name != ''){
            let allusers = firebase.firestore().collection('users'); //Get reference from firebase
            allusers.add(this.state);
            setUserDetails(this.state.email);
        } else{ 
            this.setState({ message :'Please enter your name' })  
        }
    }

   

    render() {
        const { username, userlevel } = this.props.user
        const {disciplines} = this.props.disciplines
        return(
        <>   
     
             <h1>Hi </h1>
            <p>Please enter some details below:</p>
            <Message message = {this.state.message} messageType= {this.state.messageType} />
            <pre>{JSON.stringify(this.state, null, 2) }</pre>
  
        <div >
           <form onSubmit={this.handleSubmit}>
                <label htmlFor="Name">Full Name</label>			
                <input type="text" name='Name' onChange={(e) => this.handleChange(e)} value={this.state.Name} />  
                <label htmlFor="Name">Practised disciplines (cometitive and non competitive)</label>		
                { disciplines.map((val,inx) => {
                            return (
                                < div key={val.id} >
                                     <label htmlFor="Name">{val.title}</label>	
                                    <input name="discipline" type="checkbox"  onChange= {() => this.selectCheckbox(val.title)} />
                                </div>
                            )
                })
                }
                 <UserFormdisciplines results={this.state.disciplines} addBelt={this.addBelt.bind(this)} />
                <label htmlFor="Name">Email address</label>			
                <input disabled type="text" name='username' onChange={(e) => this.handleChange(e)} value={username} />  
                <input type="submit" value="Submit" />
            </form>
        </div>

       </>
        )
    }
}

    const mapStateToProps = state => ({
        user: state.user,
        disciplines: state.discipline
    })

    export default connect(mapStateToProps)(UserFormstyled)