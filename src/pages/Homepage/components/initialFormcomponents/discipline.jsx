import React, { Component } from 'react'

const UserFormdisciplines = ({results,addBelt }) => {
    var resultsRender = results;
    return (
        <div className="discipline-list">
         <pre>{JSON.stringify(results, null, 2) }</pre>


         { resultsRender.map((val,inx) => {
             return (
                 <>
                    <h4>Details for {val.discipline}</h4>
                     <label htmlFor="Name">Belt</label>			
                     <input  type="text" name='belt' onChange={(e) => addBelt(inx, e)}  />  
                     <label htmlFor="Name">Years experience</label>	
                     <input  type="text" name='yearsExp' onChange={(e) => addBelt(inx, e)}  />  
                 </>
                 )
                })
                }
        </div>
    )
}

export default UserFormdisciplines;